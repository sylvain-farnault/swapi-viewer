require 'font-awesome-sass'

module StarWarsHelper

  def self.item_id_from_url(item_url)
    item_url.match(/\/(?<id>\d{1,3})\/$/)['id'].to_i
  end

  def self.item_category_from_url(url)
    url.match(/^https\:\/\/swapi\.dev\/api\/(?<category>\w+)/)['category']
  end

  def self.font_awesome_icon_from_category(category)
    icon_name = SwapiService::CATEGORIES[category][:font_awesome_icon]
    icon('fas', icon_name)
  end
end
