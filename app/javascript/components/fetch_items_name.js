const updateDomItemSpan = (name, itemSpan) => {
  itemSpan.querySelector('span').innerHTML = name;
  console.log('Name: ' + name);
}

const fetchItemName = (itemSpan) => {
  const apiBaseUrl = "https://swapi.dev/api";
  console.log("fetch item name");
  console.log(itemSpan);
  const url = apiBaseUrl + itemSpan.getAttribute("href");
  fetch(url)
    .then(response => response.json())
    .then(data => updateDomItemSpan(data.name || data.title, itemSpan));
}

const fetchItemsName = () => {
  console.log("fetch items name");
  const itemsSpans = document.querySelectorAll("a.item");
  if (itemsSpans) {
    itemsSpans.forEach( itemSpan => {
      fetchItemName(itemSpan);
    });
  }
}

export { fetchItemsName }