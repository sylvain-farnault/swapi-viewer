class CategoriesController < ApplicationController
  before_action :set_category, only: [:collection, :item]
  def index
  end

  def collection
    @datas = SwapiService.new(category: @category).get
    @category_icon = @datas['results'].nil? ? 'question-circle' : SwapiService::CATEGORIES[@category][:font_awesome_icon]
  end

  def item
    @id = params[:id]
    @datas = SwapiService.new(category: @category, id: @id).get
    Historic.create(name: @datas['name'] || @datas['title'], category: @category, item_id: @id)
  end

  def load_next_items
    @datas = SwapiService.new(url: params[:url]).get
    @category = StarWarsHelper.item_category_from_url(params[:url])
    respond_to do |format|
      format.js {render layout: false}
      format.html
    end
  end

  def historics
    @historics = Historic.all.order(created_at: :desc)
  end

  private

  def set_category
    @category = params[:category]
  end
end
