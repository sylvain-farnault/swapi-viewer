require 'json'
require 'open-uri'

class SwapiService
  BASE_URL = 'https://swapi.dev/api/'.freeze
  CATEGORIES = {
    'people' => { font_awesome_icon: 'street-view' },
    'planets' => { font_awesome_icon: 'globe-asia' },
    'starships' => { font_awesome_icon: 'space-shuttle' },
    'vehicles' => { font_awesome_icon: 'truck-monster' },
    'species' => { font_awesome_icon: 'code-branch' },
    'films' => { font_awesome_icon: 'film' }
  }

  def initialize(attributes)
    @category = attributes[:category]
    @id = attributes[:id]
    @url = attributes[:url]
  end

  def get
    begin
      url = @url || "#{BASE_URL}#{@category}/#{@id}"
      datas = URI.open(url).read
      result = JSON.parse(datas)
    rescue OpenURI::HTTPError => ex
      if @id.nil?
        result = {message: "There is no #{@category} in Star Wars universe!"}
      else
        result = {message: "We all dream for a new #{@category} in Star Wars universe!"}
      end
    end
    result
  end

end