Rails.application.routes.draw do
  root to: 'star_wars#categories'
  get 'load_next_items', to: "star_wars#load_next_items", as: :load_next_items
  get 'historics', to: 'star_wars#historics', as: :historics
  get ':category', to: "star_wars#category_items", as: :category
  get ':category/:id', to: "star_wars#item", as: :category_item
end
