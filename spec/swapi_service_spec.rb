require_relative '../app/services/swapi_service'

RSpec.describe SwapiService, type: :model do
  describe '#get' do
    it "Should return {message: \"...\"} if category collection does not exist" do
      test = SwapiService.new(category: 'plants').get
      expect(test[:message]).not_to be(nil)
    end
    it "Should return {message: \"...\"} if category item does not exist" do
      test = SwapiService.new(category: 'planets', id: 404).get
      expect(test[:message]).not_to be(nil)
    end
  end
end